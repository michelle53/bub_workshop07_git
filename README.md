## README
This code is part of Workshop 7 from the BU Bioinformatics Programming Workshop Task Force. There are 2 scripts in the src directory.
This is Marzie's idea.
* predict_sentiment.py - Uses machine learning to predict the sentiment of a sentence (positive or negative).
* digit_recognition_game.py - an python game where you compete with a machine learning algorithm to recognize digits.

### Required Python Modules
Python 3

### Install Required Python modules
* scikit-learn
* textblob
* matplotlib

If you have root/sudo access you can install the modules with this command:
pip install scikit-learn textblob matplotlib

without sudo access you can install it locally with:
pip install --user scikit-learn textblob

or using your a package manager like anaconda.

## Goodbye
