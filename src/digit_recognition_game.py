"""
  Authors: Vinay Kartha,Demarcus Briers. Boston University Bioinformatics Program and Michelle Patino
  Function: Simple script to predict hand written digits
"""
import time
import random
import argparse

import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import svm
from sklearn import neighbors
from sklearn import naive_bayes


def main():

    # Read command line input
    classifier_type = read_args()

    ##############################
    # Fix A. Choose a classifier #
    ##############################
    classifier = set_classifier()

    # Load the built-in dataset that comes with the sklearn module
    digits = datasets.load_digits()

    # C. Total number of digit images in dataset
    num_images = len(digits.data)
    print("Total number of digit images in dataset: %i" % num_images)

    #For a classification task, we have to define a training and test set
    #Define which images to use for training the model
    #We will then test the prediction on images outside of this
    #General rule of thumb, train on 60% of the data, and test on the remaining 40%
    num_training=1080

    #Provide information on the image data, and the true labels of the data
    x,y = digits.data[1:num_training], digits.target[1:num_training]
    classifier.fit(x,y)

    #Here, we will test the model prediction on 10 randomly selected images
    #Note that we are only choosing from the test data (that the model didn't train on)
    user_correct = 0
    machine_correct = 0
    print("Time to guess!!!\n\n")


    # Choose number of images that should be shown in the game
    num_images_to_guess = 5
    for i in random.sample(range(num_training,num_images), num_images_to_guess):
        
        #############################
        # Fix C. change figure size #
        #############################
        fig, ax = plt.subplots(figsize=(9, 9))
        plt.imshow(digits.images[i], cmap=plt.cm.gray_r, interpolation="nearest",aspect='auto')
        plt.tight_layout()
        plt.show()

        ##############################
        # Fix B. Prompt user for input
        ##############################
        human_prediction = get_human_prediction()

        # scikitlearn wants us to reshape 1D araays like this
        number_label = digits.target[i].reshape(1,-1)[0][0]
        print("The correct answer is: %s" % number_label)

        # Predict the number using your chosen classifier
        machine_prediction = classifier.predict( digits.data[i].reshape(1,-1) )[0]
        print("Our chosen model predicted: %s \n" % machine_prediction)

        #Keep track of score for humans vs machines
        if human_prediction == number_label:
            user_correct+=1
        if machine_prediction == number_label:
            machine_correct+=1

        time.sleep(1)


    ########################################
    # Print results of the competition.
    ########################################
    print("Finished!")
    print("The human identified %i numbers correctly." % user_correct)
    print("The machine correctly identified %i numbers correctly." % machine_correct)

    # Message about humans vs machines.
    if user_correct < machine_correct:
        print("Machines > humans!")
    elif user_correct == machine_correct:
        print("Humans and machines coexist")
    else:
        print("Humans > machines!")

    #Save the results
    my_results="%d,%d\n" % (user_correct,machine_correct)
    output_file = "../data/human_vs_machine.csv"
    with open(output_file, "a+") as myfile:
        myfile.write(my_results)


#####################
# Functions
####################
def read_args():
    # Read parameters from the command line
    parser = argparse.ArgumentParser(description=\
    "This is an extended Cellular Potts Model that captures self-sorting in hiPSCs.\n")
    
    #Set command line arguments
    parser.add_argument("--classifier",
                        help="Type of classifier to compete against. (SVM, KNN, or NB)",
                        type=str)
 
    # Process arguments
    args = parser.parse_args()

    return args.classifier

def set_classifier(classifier='SVM'):
    """ Set the type of classifier to use"""

    # Define ML classifier algorithms we are going to test out
    #classifier = svm.SVC(kernel="linear")  #support vector machine()
    classifier = neighbors.KNeighborsClassifier(9)  #K Nearest-Neighbors
    #classifier = naive_bayes.GaussianNB()  #Naive Bayes
    return classifier


def get_human_prediction():
    """ 
        Function: Prompts the user for the number they are guessing.
        Returns: (int) number user guessed
    """
    human_prediction = input("Type the number your saw: ")
    human_prediction = int(human_prediction)


if __name__ == '__main__':
    main()
